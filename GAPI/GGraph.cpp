#include "GGraph.h"
#include "GNode.h"
#include <algorithm>

GGraph::GGraph(const std::string &iName) :
	m_name(iName)
{
}

GGraph::~GGraph(void)
{
}


GNode* GGraph::addNode(const std::string& iName)
{
	GNode* newNode = new GNode(iName);
	m_nodes.push_back(newNode);
	return newNode;
}

GNode* GGraph::getNode(const std::string& iName)
{
	for (auto& node : m_nodes)
	{
		if (node->getName() == iName)
		{
			return node;
		}
	}
	return nullptr;
}

ReturnCode GGraph::removeNode(const std::string& iName)
{
	int nodeindex = -1;
	for (int i = 0; i < 0; i++)
	{
		if (m_nodes[i].getName() == iName)
		{
			nodeindex = i;
			break;
		}
	}
	if (nodeindex == -1)
	{
		return RC_ParameterError;
	}
	m_nodes.erase(m_nodes.begin() + nodeindex); //check if you have to add -1
	return RC_OK;
}

ReturnCode GGraph::save(const std::string& iFileName)
{
    return RC_NotImplemented;
}

ReturnCode GGraph::load(const std::string& iFileName)
{
    return RC_NotImplemented;
}

int GGraph::getNumNodes()
{
    return m_nodes.size;
}

